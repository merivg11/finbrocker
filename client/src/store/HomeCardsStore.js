import {makeAutoObservable} from 'mobx'

export default class HomeCardsStore {
    constructor(){
        this._cards = [
            {id: 1, text: 'fffffffff', group: 'dddddd', img: 'file1', icon: 'file'},
            {id: 1, text: 'aaaaaaaaaa', group: 'lllll', img: 'file1', icon: 'file'},
            {id: 1, text: 'kkkkkkkk', group: 'mmmmmmmm', img: 'file1', icon: 'file'}
        ]
        makeAutoObservable(this)
    }

    set cardsInners(cards){
        this._cards = cards
    }


    get cardsInners(){
        return this._cards
    }
}