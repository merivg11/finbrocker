import React, {createContext} from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import './css/custom.css'
import './css/index.css'
// import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import { HashRouter } from "react-router-dom";


import "./scss/volt.scss";

import "@fortawesome/fontawesome-free/css/all.css";
import "react-datetime/css/react-datetime.css";
import ScrollToTop from "./components/ScrollToTop";

import HomeCardsStore from "./store/HomeCardsStore"

export const Context = createContext(null)


ReactDOM.render(
    <Context.Provider value={{
        card: new HomeCardsStore()
    }}>
    <HashRouter>
        <ScrollToTop />
        <App />

    </HashRouter>
    </Context.Provider>,
    document.getElementById("root")
);

reportWebVitals();