import React from 'react';

const Footer = () => {
    return (
        <footer>
            <div className="footer-wrapper">
                <div className="container footer">
                    <div className="footer-left">
                        <div className="menu-first-list">
                            <div className="menu-item">
                                <a href="#">Главная</a>
                            </div>
                            <div className="menu-item">
                                <a href="#">О брокере</a>
                            </div>
                            <div className="menu-item menu-item-select">
                                <a href="#">Продукты</a>
                                <div className="menu-item-sub">
                                    <a href="#">Потребительский кредит</a>
                                    <a href="#">Денежный кредит</a>
                                    <a href="#">Страховые продукты</a>
                                    <a href="#">Карточные продукты</a>
                                </div>
                            </div>
                            <div className="menu-item menu-item-select">
                                <a href="#">Преимущества</a>
                                <div className="menu-item-sub">
                                    <a href="#">Для банков</a>
                                    <a href="#">Для ритейла</a>
                                </div>
                            </div>
                        </div>
                        <div className="menu-second-list">
                            <div className="menu-item menu-item-select">
                                <a href="#">Партнеры</a>
                                <div className="menu-item-sub">
                                    <a href="#">Финансовые</a>
                                    <a href="#">Торговые</a>
                                    <a href="#">Партнеры о нас</a>
                                </div>
                            </div>
                            <div className="menu-item">
                                <a href="#">Новости</a>
                            </div>
                            <div className="menu-item">
                                <a href="#">Контакты</a>
                            </div>
                        </div>
                    </div>
                    <div className="footer-right">
                        <div className="footer-contact">
                            <div className="icon-group">
                                <div className="icon-phone"></div>
                                <a href="tel:+380503830829">+38 (050) 383-08-29</a>
                            </div>
                            <div className="icon-group">
                                <div className="icon-mail"></div>
                                <a href="mailto:broker-sup@sfr.kiev.ua">Broker-sup@sfr.kiev.ua</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="copyright">
                    © Copyright 2015 Финансовый брокер
                </div>
            </div>
        </footer>
    );
};

export default Footer;