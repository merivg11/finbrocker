import React, { useState } from "react";
import SimpleBar from 'simplebar-react';
import { useLocation } from "react-router-dom";
import { CSSTransition } from 'react-transition-group';
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faSignOutAlt, faTimes} from "@fortawesome/free-solid-svg-icons";
import {Nav, Badge, Image, Button, Dropdown, Accordion, Navbar} from '@themesberg/react-bootstrap';
import { Link } from 'react-router-dom';

import { Routes } from "../../routes";


export default (props = {}) => {
    const location = useLocation();
    const { pathname } = location;
    const [show, setShow] = useState(false);
    const showClass = show ? "show" : "";
    const [showDefault, setShowDefault] = useState(false);
    const handleClose = () => setShowDefault(false);
    const onCollapse = () => setShow(!show);

    // const CollapsableNavItem = (props) => {
    //     const { eventKey, title, icon, children = null } = props;
    //     const defaultKey = pathname.indexOf(eventKey) !== -1 ? eventKey : "";
    //
    //     return (
    //         <Accordion as={Nav.Item} defaultActiveKey={defaultKey}>
    //             <Accordion.Item eventKey={eventKey}>
    //                 <Accordion.Button as={Nav.Link} className="d-flex justify-content-between align-items-center">
    //         <span>
    //           <span className="sidebar-icon"> </span>
    //           <span className="sidebar-text">{title}</span>
    //         </span>
    //                 </Accordion.Button>
    //                 <Accordion.Body className="multi-level">
    //                     <Nav className="flex-column">
    //                         {children}
    //                     </Nav>
    //                 </Accordion.Body>
    //             </Accordion.Item>
    //         </Accordion>
    //     );
    // };

    const NavItem = (props) => {
        const { title, link, external, target, icon, image, badgeText, badgeBg = "secondary", badgeColor = "primary" } = props;
        const classNames = badgeText ? "d-flex justify-content-start align-items-center justify-content-between" : "";
        const navItemClassName = link === pathname ? "active" : "";
        const linkProps = external ? { href: link } : { as: Link, to: link };

        return (
            <Nav.Item className={navItemClassName} onClick={() => setShow(false)}>
                <Nav.Link {...linkProps} target={target} className={classNames}>
          <span>
            {icon ? <span className="sidebar-icon"> </span> : null}
              {image ? <Image src={image} width={20} height={20} className="sidebar-icon svg-icon" /> : null}

              <span className="sidebar-text">{title}</span>
          </span>
                    {badgeText ? (
                        <Badge pill bg={badgeBg} text={badgeColor} className="badge-md notification-count ms-2">{badgeText}</Badge>
                    ) : null}
                </Nav.Link>
            </Nav.Item>
        );
    };

    return (
        <>
            <Navbar expand={false} collapseOnSelect variant="dark" className="navbar-theme-primary px-4 d-md-none">
                <Navbar.Toggle as={Button} aria-controls="main-navbar" onClick={onCollapse}>
                    <span className="navbar-toggler-icon" />
                </Navbar.Toggle>
            </Navbar>
            <CSSTransition timeout={300} in={show} classNames="sidebar-transition">
                <SimpleBar className={`collapse ${showClass} sidebar d-md-block bg-primary text-white`}>
                    <div className="sidebar-inner px-4 pt-3">
                        <Nav className="flex-column pt-3 pt-md-0">
                            <Dropdown.Divider className="my-3 border-indigo" />
                            <NavItem title="Главная" link={Routes.HomePageEditAdm.path} />
                            <NavItem title="Финансовые партнёры" link={Routes.FinPartnersPageAdmin.path} />
                            <NavItem title="О брокере" link={Routes.AboutPageAdmin.path} />
                            <NavItem title="Потребительский кредит" link={Routes.CustCredPageAdmin.path} />
                            <NavItem title="Денежный кредит" link={Routes.FinCredPageAdmin.path} />
                            <NavItem title="Страховые продукты" link={Routes.InsuranceProdPageAdmin.path} />
                            <NavItem title="Карточные продукты" link={Routes.CardProdPageAdmin.path} />
                            <NavItem title="Преимущества для банков" link={Routes.BanksBenefPageAdmin.path} />
                            <NavItem title="Преимущества для ритейла" link={Routes.RetailBenefPageAdmin.path} />
                            <NavItem title="Торговые партнёры" link={Routes.TrPartnersPageAdmin.path} />
                            <NavItem title="Партнеры о нас" link={Routes.PartnAboutPageAdmin.path} />
                            <NavItem title="Новости" link={Routes.NewsPageAdmin.path} />
                            <NavItem title="Контакты" link={Routes.ContactsPageAdmin.path} />

                            {/*<NavItem title="Главная" link={Routes.HomePageEditAdmin.path} />*/}
                            {/*<NavItem title="Финансовые партнёры" link={Routes.DocsDownload.path} />*/}
                            {/*<NavItem title="О брокере" link={Routes.AdmAbout.path} />*/}
                            {/*<NavItem title="Потребительский кредит" link={Routes.DocsLicense.path} />*/}
                            {/*<NavItem title="Денежный кредит" link={Routes.DocsFolderStructure.path} />*/}
                            {/*<NavItem title="Страховые продукты" link={Routes.DocsBuild.path} />*/}
                            {/*<NavItem title="Карточные продукты" link={Routes.DocsChangelog.path} />*/}
                            {/*<NavItem title="Преимущества для банков" link={Routes.Accordions.path} />*/}
                            {/*<NavItem title="Преим для ритейла" link={Routes.Alerts.path} />*/}
                            {/*<NavItem title="Торговые партнёры" link={Routes.Badges.path} />*/}
                            {/*<NavItem title="Партнеры о нас" link={Routes.Breadcrumbs.path} />*/}
                            {/*<NavItem title="Новости" link={Routes.Buttons.path} />*/}
                            {/*<NavItem title="Контакты" link={Routes.Forms.path} />*/}
                        </Nav>
                    </div>
                </SimpleBar>
            </CSSTransition>
        </>
    );
};
