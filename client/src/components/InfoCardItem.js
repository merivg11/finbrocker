import React from 'react';
import '../css/index.css'

const InfoCardItem = () => {
    return (
        <div className="welcome-why-item">
            <div className="welcome-why-item__header">
                Выгоднее
            </div>
            <div className="welcome-why-item__sub">
                реализовывать концепцию лояльности, значительно подняв уровень сервиса в торговой точке.
            </div>
        </div>
    );
};

export default InfoCardItem;