import React from 'react';
import InfoCardItem from "./InfoCardItem";
import '../css/index.css'

const InfoCardList = () => {
    return (
        <div className="welcome-why-wrapper">
            <div className="container" style={{position: 'relative'}}>
                <div className="welcome-why">
                    <InfoCardItem />
                    <InfoCardItem />
                    <InfoCardItem />
                    <InfoCardItem />
                </div>
            </div>
        </div>

    );
};

export default InfoCardList;