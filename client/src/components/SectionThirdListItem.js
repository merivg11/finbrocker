import React from 'react';

const SectionThirdListItem = () => {
    return (
        <div className="section-third-list__item">
            <div className="section-third-list__icon">
                <span className="icon-wallet"></span>
            </div>
            <div className="section-third-list__message">
                Внедрить инновационные решения в товарном и денежном кредитовании, страховых и карточных
                продуктах.
            </div>
        </div>
    );
};

export default SectionThirdListItem;