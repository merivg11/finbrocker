import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css'
import '../css/custom.css'

const SectionSecond = () => {
    return (
        <div className="section-second-wrapper">
            <div className="container">
                <div className="section-second">
                    <div className="section-second-info">
                        <div className="section-second-info-wrapper">
                            <div className="section-second-info__header">
                                Наша цель – изменить рынок кредитования к лучшему!
                            </div>
                            <div className="section-second-info__sub">
                                И своими результатами показать, что «Финансовый Брокер» – совершенная выгода как для
                                продавцов и
                                кредиторов, так и потребителей!
                            </div>
                        </div>
                    </div>
                    <div className="section-second-message">
                        <ul>
                            <li>
                                «Финансовый Брокер» (от слова «брокер» – посредник, который помогает осуществлению
                                сделок
                            </li>
                            <li>
                                между заинтересованными сторонами) – бизнес-проект по эффективной организации
                            </li>
                            <li>
                                процесса потребительского экспресс-кредитования непосредственно в точках продаж.
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionSecond;