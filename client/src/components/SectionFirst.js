import React from 'react';
import firstSectionImage from '../images/1.png'
import '../css/index.css'

const SectionFirst = () => {
    return (
        <div className="welcome-wrapper">
            <div className="container">
                <div className="welcome">
                    <div className="welcome-main">
                        <div className="welcome-title">
                            <div className="welcome-title__header">
                                Инновационные решения в потребительском кредитовании
                            </div>
                            <div className="welcome-title__sub">
                                Полностью автоматизированое экспресс-кредитование с помощью «Финансового Брокера»
                            </div>
                            <div className="welcome-title__header_sm_screen">
                                Потребительский кредит
                            </div>
                            <div className="welcome-title__sub_sm_screen">
                                Экспресс-кредитование с помощью
                                «Финансового Брокера»
                            </div>
                        </div>
                        <div className="welcome-images">
                            <img src={firstSectionImage}/>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionFirst;