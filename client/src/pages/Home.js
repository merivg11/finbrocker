import React from 'react';
import Header from "../components/Header";
import SectionFirst from "../components/SectionFirst";
import InfoCardList from "../components/InfoCardList";
import SectionSecond from "../components/SectionSecond";
import SectionSecondSmScreen from "../components/SectionSecondSmScreeen";
import SectionThird from "../components/SectionThird";
import SectionFourth from "../components/SectionFourth";
import Footer from "../components/Footer";


const Home = () => {
    return (
        <div>
            <Header />
            <SectionFirst />
            <InfoCardList />
            <SectionSecond />
            <SectionSecondSmScreen />
            <SectionThird />
            < SectionFourth />
            <Footer />
        </div>
    );
};

export default Home;