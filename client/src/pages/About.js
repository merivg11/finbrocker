import React from 'react';
import '../css/contact.css'
import '../css/custom.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import '../css/index.css'
import Header from '../components/Header'
import Footer from '../components/Footer'


const About = () => {
    return (
        <div>
            <Header />
            <main>
                <div className="breadcrumb_content_wrapper">
                    <div className="container h-100">
                        <div className="breadcrumb_content">
                            <div className="breadcrumb_content__header">
                                О брокере
                            </div>
                            <nav aria-label="breadcrumb">
                                <ol className="breadcrumb">
                                    <li className="breadcrumb-item"><a href="#">Главная</a></li>
                                    <li className="breadcrumb-item active" aria-current="page">О брокере</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>




                <div className="container">
                    <div className="content">
                        <h2 className="heading2 my-3">
                            «Финансовый Брокер» предоставляет услуги по оформлению экспресс-займа наличными.
                        </h2>
                        <h3 className="heading3">
                            Прогрессивная технология обслуживания покупателей от «Финансового Брокера» позволяет:
                        </h3>
                        <ul className="list">
                            <li>комфортно организовать работу точки выдачи потребительских кредитов в магазине (вместо 5
                                экспертов,
                                предоставляющих кредитные услуги от разных банков, в магазине будет один финансовый
                                брокер, который
                                работает
                                с кредитными предложениями от 10 учреждений);
                            </li>
                            <li> ускорить процесс оформления займа покупателями (на обслуживание одной клиентской заявки
                                нашему
                                брокеру
                                нужно всего 15 минут);
                            </li>
                            <li> эффективно подойти к привлечению финансовых партнеров (отныне количество
                                банков-партнеров будет
                                зависеть не
                                от ваших возможностей, а от ваших желаний, а брокер совместно с покупателем будет
                                выбирать из обилия
                                их
                                кредитных предложений наиболее подходящие);
                            </li>
                            <li> выгодней реализовывать концепцию лояльности, значительно подняв уровень сервиса в
                                торговой точке.
                                Мы
                                решаем
                                основную проблему посетителя – организовываем удобный процесс получения желаемого
                                результата., Ваш
                                бизнес
                                непременно станет объектом преданности со стороны клиента и его семьи, так что
                                последующие покупки
                                они
                                будут
                                совершать именно в вашей, благожелательной и чуткой к потребностям посетителей торговой
                                точке.
                            </li>
                        </ul>

                    </div>
                </div>





            </main>
            <Footer />
        </div>
    );
};

export default About;