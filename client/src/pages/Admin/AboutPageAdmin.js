import React from 'react';
import {Row, Col, Card, Container, Button} from '@themesberg/react-bootstrap';
import {Link} from "react-router-dom";
import {Routes} from "../../routes";


const AboutPageAdmin = () => {
    return (
        <Container className="px-0">
            <Row>
                <Col xs={12} className="p-3">
                    <Card>
                        <Card.Body>
                            <article>
                                <h1 className="h2" id="overview">Компоненты страницы </h1>


                                <p>Выберите компонент для редактирования:</p>

                                <Button as={Link} to={Routes.DocsEditAbout.path} variant="primary"
                                        className="my-3 mx-2">Текст</Button>
                                
                            </article>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )
}
export default AboutPageAdmin