import React from 'react';
import {Row, Col, Card, Container, Button} from '@themesberg/react-bootstrap';
import {Link} from "react-router-dom";
import {Routes} from "../../../routes";
import {Form} from '@themesberg/react-bootstrap';
// import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
// import {faRocket} from "@fortawesome/free-solid-svg-icons";
//
// import script from "../../script";
// import handleFormSubmit from "../../script";

const CardsHomePageEdit = () => {
    return (
        <Container className="px-0">
            <Row>
                <Col xs={12} className="p-3">
                    <Card>
                        <Card.Body>

                            <h1 className="h2">Карточка 1</h1>
                            <Form>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section title</Form.Label>
                                    <Form.Control as="textarea" rows="2"/>
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section text</Form.Label>
                                    <Form.Control as="textarea" rows="3" id="text"/>
                                </Form.Group>
                            </Form>

                            <h1 className="h2">Карточка 2</h1>
                            <Form>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section title</Form.Label>
                                    <Form.Control as="textarea" rows="2"/>
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section text</Form.Label>
                                    <Form.Control as="textarea" rows="3" id="text"/>
                                </Form.Group>
                            </Form>

                            <h1 className="h2">SКарточка 3</h1>
                            <Form>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section title</Form.Label>
                                    <Form.Control as="textarea" rows="2"/>
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section text</Form.Label>
                                    <Form.Control as="textarea" rows="3" id="text"/>
                                </Form.Group>
                            </Form>

                            <h1 className="h2">Карточка 4</h1>
                            <Form>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section title</Form.Label>
                                    <Form.Control as="textarea" rows="2"/>
                                </Form.Group>
                                <Form.Group className="mb-3">
                                    <Form.Label>Section text</Form.Label>
                                    <Form.Control as="textarea" rows="3" id="text"/>
                                </Form.Group>
                            </Form>


                            <Button as={Link} to={Routes.Upgrade.path} variant="primary" className="my-3" /*onClick={

                                handleFormSubmit}*/>Применить</Button>

                            <Button as={Link} to={Routes.HomePrev.path} variant="primary" className="my-3 ms-3" /*onClick={

                                handleFormSubmit}*/>Превью</Button>

                            <Button as={Link} to={Routes.HomePageEditAdm.path} variant="primary" className="my-3 ms-3" /*onClick={

                                handleFormSubmit}*/>Отмена</Button>


                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>)
}

export default CardsHomePageEdit;