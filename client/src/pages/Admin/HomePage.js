import React, { useState, useEffect, useContext } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import { Routes } from "../../routes";

import HomePageEditAdm from "./HomePageEditAdm"
import S1TitleHomePageAdmin from "./HomePageElem/S1TitleHomePageAdmin";
import CardsHomePageEdit from "./HomePageElem/CardsHomePageEdit";
import S2TexteHomePageAdmin from "./HomePageElem/S2TexteHomePageAdmin";



import Upgrade from "./Upgrade";
import NotFoundPage from "./NotFound";
import ServerError from "./ServerError";

// documentation pages
import FinPartnersPageAdmin from "./FinPartnersPageAdmin";

import DocsEditAbout from "./AboutPageElem/DocsEditAbout";
import AboutPageAdmin from "./AboutPageAdmin";
import CustCredPageAdmin from "./CustCredPageAdmin";
import FinCredPageAdmin from "./FinCredPageAdmin";
import InsuranceProdPageAdmin from "./InsuranceProdPageAdmin";
import CardProdPageAdmin from "./CardProdPageAdmin";
import NewsPageAdmin from "./NewsPageAdmin"

// components
import Sidebar from '../../components/admin/Sidebar';
// import Navbar from '../../components/admin/Navbar';
import Preloader from '../../components/admin/Preloader';

import BanksBenefPageAdmin from '../../pages/Admin/BanksBenefPageAdmin';
import RetailBenefPageAdmin from '../../pages/Admin/RetailBenefPageAdmin';
import TrPartnersPageAdmin from '../../pages/Admin/TrPartnersPageAdmin';
import PartnAboutPageAdmin from '../../pages/Admin/PartnAboutPageAdmin';
// import Buttons from '../../components/admin/Buttons';
import ContactsPageAdmin from '../../pages/Admin/ContactsPageAdmin';




//main
import Home from '../Home'
import HomePrev from '../HomePrev'
import About from '../About'


const RouteWithLoader = ({ component: Component, ...rest }) => {
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        const timer = setTimeout(() => setLoaded(true), 1000);
        return () => clearTimeout(timer);
    }, []);

    return (
        <Route {...rest} render={props => ( <> <Preloader show={loaded ? false : true} /> <Component {...props} /> </> ) } />
    );
};

const RouteWithSidebar = ({ component: Component, ...rest }) => {
    const [loaded, setLoaded] = useState(false);

    useEffect(() => {
        const timer = setTimeout(() => setLoaded(true), 1000);
        return () => clearTimeout(timer);
    }, []);

    // const localStorageIsSettingsVisible = () => {
    //     return localStorage.getItem('settingsVisible') === 'false' ? false : true
    // }
    //
    // const [showSettings, setShowSettings] = useState(localStorageIsSettingsVisible);
    //
    // const toggleSettings = () => {
    //     setShowSettings(!showSettings);
    //     localStorage.setItem('settingsVisible', !showSettings);
    // }

    return (
        <Route {...rest} render={props => (
            <>
                <Preloader show={loaded ? false : true} />
                <Sidebar />

                <main className="content">
                    <Component {...props} />
                </main>
            </>
        )}
        />
    );
};

 const HomePage = () => {
    return(
    <Switch>
        <RouteWithLoader exact path={Routes.NotFound.path} component={NotFoundPage} />
        <RouteWithLoader exact path={Routes.ServerError.path} component={ServerError} />
        {/* pages */}
        <RouteWithSidebar exact path={Routes.Upgrade.path} component={Upgrade} />

        {/* documentation */}
        <RouteWithSidebar exact path={Routes.HomePageEditAdm.path} component={HomePageEditAdm} />
        <RouteWithSidebar exact path={Routes.S1TitleHomePageAdmin.path} component={S1TitleHomePageAdmin} />
        <RouteWithSidebar exact path={Routes.CardsHomePageEdit.path} component={CardsHomePageEdit} />
        <RouteWithSidebar exact path={Routes.S2TexteHomePageAdmin.path} component={S2TexteHomePageAdmin} />
        <RouteWithSidebar exact path={Routes.DocsEditAbout.path} component={DocsEditAbout} />

        <RouteWithSidebar exact path={Routes.BanksBenefPageAdmin.path} component={BanksBenefPageAdmin} />
        <RouteWithSidebar exact path={Routes.RetailBenefPageAdmin.path} component={RetailBenefPageAdmin} />
        <RouteWithSidebar exact path={Routes.TrPartnersPageAdmin.path} component={TrPartnersPageAdmin} />
        <RouteWithSidebar exact path={Routes.PartnAboutPageAdmin.path} component={PartnAboutPageAdmin} />
        <RouteWithSidebar exact path={Routes.NewsPageAdmin.path} component={NewsPageAdmin} />
        <RouteWithSidebar exact path={Routes.ContactsPageAdmin.path} component={ContactsPageAdmin} />

        <RouteWithSidebar exact path={Routes.FinPartnersPageAdmin.path} component={FinPartnersPageAdmin} />
        <RouteWithSidebar exact path={Routes.AboutPageAdmin.path} component={AboutPageAdmin} />
        <RouteWithSidebar exact path={Routes.CustCredPageAdmin.path} component={CustCredPageAdmin} />
        <RouteWithSidebar exact path={Routes.FinCredPageAdmin.path} component={FinCredPageAdmin} />
        <RouteWithSidebar exact path={Routes.InsuranceProdPageAdmin.path} component={InsuranceProdPageAdmin} />
        <RouteWithSidebar exact path={Routes.CardProdPageAdmin.path} component={CardProdPageAdmin} />
        <RouteWithLoader exact path={Routes.Home.path} component={Home} />
        <RouteWithSidebar exact path={Routes.HomePrev.path} component={HomePrev} />
        <RouteWithSidebar exact path={Routes.About.path} component={About} />
        
        <Redirect to={Routes.NotFound.path} />
    </Switch>
)}

export default HomePage